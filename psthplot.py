import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# Animated plotter for raster and PSTH. Updated from main.py

def animate_psth(dat, dq):
	fig, ax, rasters = plot_psth_base(dat.get_plot_meta(), dat.peritime)
	fig.tight_layout(pad=2.5)
	anim = animation.FuncAnimation(fig, update_psth, fargs=(dat, ax, rasters, dq), interval=1000)
	plt.show()

def plot_psth_base(plotmeta, timelimits):
	conds = plotmeta[0]
	cells = plotmeta[1]
	nconds = len(conds)
	ncells = len(cells)
	fig, ax = plt.subplots(nconds*2, ncells, sharex='col', figsize=(4*ncells, 7*nconds))
	rasters = [[None]*ncells for i in range(nconds)]

	dictlister = lambda x: x if type(x) == list else list(x.values())

	for j in range(ncells):
		for i in range(nconds):
			histax = ax[i*2][j]
			rastax = ax[i*2+1][j]

			histax.set_title("Cell {}".format(dictlister(plotmeta[1])[j]))
			histax.set_ylabel('Spikes')
			histax.axis('tight')
			histax.hist([])

			rastax.axis('off')
			rastax.set_ylim([-1, 8])
			rasters[i][j], = rastax.plot([], [], 'k|', markersize=5)
	return fig, ax, rasters

def update_psth(num, dat, ax, rasters, dq):

	ax_redraw_set = set()
	dictlister = lambda x: x if type(x) == list else list(x.values())
	keylister = lambda x: x if type(x) == list else list(x.keys())

	if dq.qsize() > 0:
		try:
			# collect all the axes waiting for draw
			for i in range(dq.qsize()):
				drawreq = dq.get_nowait()
				if drawreq is not None:
					ax_redraw_set.add(drawreq)

			plotmeta = dat.get_plot_meta()

			for ax_i in ax_redraw_set:
				try:
					cond_ax = keylister(plotmeta[0]).index(ax_i[0])
					cell_ax = keylister(plotmeta[1]).index(ax_i[1])
				except:
					raise ValueError('Axis for condition or cell not found')

				histax = ax[cond_ax*2, cell_ax]
				rastline = rasters[cond_ax][cell_ax]

				allrasters = dat.get_segment(ax_i[0], ax_i[1])
				histdraw = allrasters['DeltaTime']

				histax.cla()
				histax.set_title("Cell {}".format(dictlister(plotmeta[1])[ax_i[1]]))
				histax.set_ylabel('Spikes')
				histax.axis('tight')
				histax.hist(histdraw)

				last8rasters = allrasters[allrasters['TrialCount'] > allrasters.iloc[-1]['TrialCount'] - 8]
				xdraw = last8rasters['DeltaTime']
				ydraw = allrasters.iloc[-1]['TrialCount'] - last8rasters['TrialCount']
				rastline.set_data(xdraw, ydraw)
		except KeyboardInterrupt:
			return
# pyECubeRaster

This is an example framework for a python-based [ECube](https://white-matter.com/products/ecubeserver/) and [OpenEphys Spike Sorter](https://github.com/open-ephys/plugin-GUI/tree/master/Plugins/SpikeSorter) + EventBroadcaster real-time [peristimulus time histogram](https://en.wikipedia.org/wiki/Peristimulus_time_histogram) and raster plotting.

## Where do I start?

If you have not already, download and install [Anaconda](https://www.anaconda.com/distribution/) for your desired platform. If you are running 64-bit Windows, run `python main.py` to start the plotting process. If you are using another platform, you may need to build the `pdibitconv` Cython module with `python setup.py build_ext --inplace`.

Start in [main.py](main.py), which shows the basic configs and setup between multiple threads.

Edit the configuration to suit your setup, as well as the allowed digital panel input states and cell IDs sorted.

The main streaming components are:

- [conditionevents](conditionevents.py): for an example in reading raw bit states from the front digital panel from eCube API
- [spikeevents](spikeevents.py): example in reading in spikes sorted by OpenEphys SpikeSorter (and broadcasted via EventBroadcaster)
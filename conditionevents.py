import comtypes.client
import os.path
import sys
import time
import pdibitconv

# Reading raw bit states from the front digital panel from eCube API

class eCubePDI:
	def __init__(self, address=None):
		###### Start eCube connection process #####

		# Connect to Windows OLE / COM API for eCube

		try:
			self.hecube = comtypes.client.CreateObject('ecubeapi.Ecube.1')
		except:
			raise SystemExit('Cannot connect to eCube API, please make sure eCube is installed.')

		# Scan for devices

		detdevs = self.hecube.DetectNetworkDevices()
		if len(detdevs) < 1:
			raise ValueError('No eCube detected.')
		elif len(detdevs) > 1:
			if address is None:
				print("{} eCubes detected. When initializing {}.eCubePDI() please specify parameter address='x.x.x.x'".format(len(detdevs), os.path.basename(__file__)))
				print("Available eCubes:")
				for devname in detdevs:
					print("    {}".format(devname))
				sys.exit()
		else:
			if address is None:
				address = detdevs[0]

		if address is not None:
			if address not in detdevs:
				print("Warning: specified address {} not in list of available eCubes: {}".format(address, detdevs))

		# Connect to specified device
		try:
			self.hdev = self.hecube.OpenNetworkDevice(address);
		except:
			raise SystemExit('Cannot connect to listed eCube. Is it already connected?')

		# Connect to PDI
		try:
			self.hdio = self.hdev.OpenModule('PanelDigitalIO')
		except:
			raise SystemExit('Connected eCube failed to start PanelDigitalIO. Is it already in use?')

		# list available ports
		try:
			self.hprts = list(map(lambda x: self.hdio.OpenChannel(x), self.hdio.Channels))
			self.chids = list(map(lambda x: x.ID, self.hprts))
		except:
			raise SystemExit('Connected PanelDigitalIO failed to connect to ports. Is it already in use?')

		if(len(self.chids)) != 6:
			raise ValueError('PanelDigitalIO shows incorrect number of channels.')

		# create the streaming interface in API
		self.stream = self.hecube.CreateDigitalInputStreaming(self.hprts[0])
		for prt in self.hprts[1:]:
			self.stream.AddChannel(prt)

		self.latestportvals = [None]*6
		self.lateststate = 0

		# set up the digital input port value -> raw bitmap converter
		self.__init_digital_conversion__()

		self.trialcounter = dict()

	def __init_digital_conversion__(self):
		self.pdiconverter = pdibitconv.PDIConverter()


	def start(self):
		self.stream.Start()


	def stop(self):
		self.stream.Stop()


	def stream_detectstates(self, q):
		while True:
			changed = False
			ba = self.stream.BuffersAcquired

			if ba > 0:
				for j in range(ba):
					hbuf = self.stream.FetchNextBuffer()
					if self.latestportvals[hbuf.StreamID-6432] != hbuf.DataSamples[-1]:
						self.latestportvals[hbuf.StreamID-6432] = hbuf.DataSamples[-1]
						changed = True
					
					if len(self.latestportvals) != 6:
						raise ValueError('Latest state corrupt')

					if changed and all(v is not None for v in self.latestportvals):
						prevstate = self.lateststate
						self.lateststate = self.pdiconverter.convert_single(self.latestportvals)

						if self.lateststate in self.trialcounter:
							self.trialcounter[self.lateststate] += 1
						else:
							self.trialcounter[self.lateststate] = 0

						q.put((1, time.monotonic(), self.lateststate, prevstate, self.trialcounter[self.lateststate]))
			else:
				time.sleep(0.001)
import eventstream
import spikeevents
import conditionevents
import psthplot

import threading
import queue

# configs
eCubeAddress = '192.168.128.190' # IP Address for eCube or ServerNode, for directly connecting and reading DigitalPanel via the eCube API
openephysAddress = '192.168.128.190' # OpenEphys ~0.4.2-0.4.3 EventBroadcaster module address after spike sorting
conditions = {0: 'Rest', 1: 'LeftPaw', 2: 'RightPaw', 3: 'Lick', 4: 'Eat'} # Example conditions: numbers correspond to bits set on the ECube input panel
sortedcells = {0: 'Unsorted', 1: 'Hippo_1', 2: 'Hippo_2', 3: 'V1_1', 4: 'V1_2', 5: 'Blah', 6: 'Blah2'} # Example sorted cells: numbers correspond to numbers from OpenEphys spike sorter module
peristimulus_times = (-1, 2) # example: -1 second to +2 second around stimulus condition onset (rising edge)


# See conditionevents.py for an example in reading raw bit states from the front digital panel from eCube API
# See spikeevents.py for an example in reading in spikes sorted by OpenEphys SpikeSorter (and broadcasted via EventBroadcaster)


q = queue.Queue() # queue for spikes and condition changes
drawq = queue.Queue() # queue to notify subplot updates
dat = eventstream.EventStream(condnames=conditions, cellnames=sortedcells, peritime=peristimulus_times) # stream dataframe and methods

# connect to and start the eCube front panel
pdi = conditionevents.eCubePDI(address=eCubeAddress)
pdi.start()

# start the network zeromq thread for picking up OpenEphys spike sorter from EventBroadcaster
spikethread = threading.Thread(target=spikeevents.receive, args=(q, openephysAddress))
spikethread.start()

# start the eCube front panel eCube API reading thread
condthread = threading.Thread(target=pdi.stream_detectstates, args=(q,))
condthread.start()

# synchronize incoming events in queue to dataframe
updatethread = threading.Thread(target=dat.update_eventstream, args=(q, drawq))
updatethread.start()

psthplot.animate_psth(dat, drawq)